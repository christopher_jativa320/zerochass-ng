// Get the dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

// Get the API routes
const api = require('./server/routes/api');

// Initialize Express
const app = express();

// The parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// Point our static path to the 'src' directory
app.use(express.static(path.join(__dirname, 'dist')));

// Set up our api routes
app.use('/api', api);

// Catch all our routes and return the index file
app.get('*', (request, response) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 *  Get the port from the environment and store it in Express
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create our HTTP server
 */
const server = http.createServer(app);

/**
 * Listen on the provided port on all network interfaces
 */
server.listen(port, () => console.log('API running on localhost:${port}'));