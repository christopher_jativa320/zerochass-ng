import { NgModule } from '@angular/core';
import { MatToolbarModule, MatButtonModule, MatInputModule, MatDialogModule, MatListModule, MatSidenavModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material';

@NgModule({
  imports: [MatToolbarModule, MatButtonModule, MatInputModule, MatDialogModule, MatListModule, MatMenuModule,
    MatSidenavModule, BrowserAnimationsModule],
  exports: [MatToolbarModule, MatButtonModule, MatInputModule, MatDialogModule, MatListModule, MatMenuModule,
    MatSidenavModule, BrowserAnimationsModule],
})
export class AppMaterialComponentsModule { }
