import { Component, OnInit, Inject, OnChanges, ViewChild } from '@angular/core';
import { SignInComponent } from '../authentication/sign-in/sign-in.component';
import { MatDialog, MatDialogRef, MatMenuTrigger } from '@angular/material';
import { UserServicesService } from '../../services/user-services.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnChanges {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  public isUserLoggedIn;

  constructor(public dialog: MatDialog, public us: UserServicesService) {
    this.us.af.auth.onAuthStateChanged((user) => {
      if (user) {
        this.isUserLoggedIn = true;
      } else {
        this.isUserLoggedIn = false;
      }
    });
  }

  closeMatMenu() {
    this.trigger.closeMenu();
  }

  openMatMenu() {
    this.trigger.openMenu();
  }

  handleLogin() {
    const dialogRef = this.dialog.open(SignInComponent, {
      data: {
        login: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  handleSignUp() {
    const dialogRef = this.dialog.open(SignInComponent, {
      data: {
        login: false
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  handleLogout() {
    this.us.logout();
  }

  ngOnChanges() {

  }

  ngOnInit() {
  }
}
