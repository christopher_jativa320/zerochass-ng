import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {

  // Determine whether the login controls or the sign-up controls should be displayed in the dialog
  isLoginDialog = false;

  // Forms for logging in and signing up
  loginForm: FormGroup;
  signUpForm: FormGroup;


  constructor(private formBuilder: FormBuilder, public router: Router,
    public dialogRef: MatDialogRef<SignInComponent>, private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.isLoginDialog = data.login;
    this.createForms();
  }

  createForms() {

    // Build the login form
    this.loginForm = this.formBuilder.group({
      'email-address': [null, Validators.compose([
        Validators.required, Validators.minLength(3), Validators.maxLength(30)]
      )],
      'password': [null, Validators.compose([
        Validators.required, Validators.minLength(6), Validators.maxLength(100)
      ])]
    });

    // Build the sign-in form
    this.signUpForm = this.formBuilder.group({
      'name': [null, Validators.compose([
        Validators.required, Validators.minLength(3), Validators.maxLength(30)]
      )],
      'email-address': [null, Validators.compose([
        Validators.required, Validators.minLength(3), Validators.maxLength(30)]
      )],
      'password': [null, Validators.compose([
        Validators.required, Validators.minLength(6), Validators.maxLength(100)
      ])]
    });
  }

  login(): void {

    let response;

    // Get the values from our sign up form
    const email = this.loginForm.get('email-address').value;
    const password = this.loginForm.get('password').value;


    this.http
      .post('/api/login', { email: email, password: password })
      .subscribe(data => response = data);

    this.closeDialog();

  }

  logout(): void {
    // this.userServices.logout();
  }

  displayLoginControls(): void {
    this.isLoginDialog = true;
  }

  displaySignUpControls(): void {
    this.isLoginDialog = false;
  }

  signUp(): void {

    let response;

    // Get the values from our sign up form
    const email = this.signUpForm.get('email-address').value;
    const name = this.signUpForm.get('name').value;
    const password = this.signUpForm.get('password').value;

    this.http
      .post('/api/sign-up', { name: name, email: email, password: password })
      .subscribe(data => response = data);


    this.closeDialog();
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}
