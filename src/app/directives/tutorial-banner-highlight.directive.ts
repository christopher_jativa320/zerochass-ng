import { Directive, Input, OnInit, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: '[appTutorialBannerHighlight]'
})
export class TutorialBannerHighlightDirective implements OnInit {

  @Input() index: any;

  color: string;
  re: Renderer2;
  el: ElementRef;

  constructor(re: Renderer2, el: ElementRef) {
    this.re = re;
    this.el = el;
  }

  ngOnInit() {
    this.generateBannerColor(this.index);
  }

  generateBannerColor(i) {
    console.log();

    i = i + 1;

    switch (i) {

      case 1:
        this.re.addClass(this.el.nativeElement, 'li-black');
        break;

        case 2:
        this.re.addClass(this.el.nativeElement, 'li-pink');
        break;

        case 3:
        this.re.addClass(this.el.nativeElement, 'li-white');
        break;

        case 4:
        this.re.addClass(this.el.nativeElement, 'li-teal');
        break;

        case 5:
        this.re.addClass(this.el.nativeElement, 'list-purple');
        break;
    }
  }
}
