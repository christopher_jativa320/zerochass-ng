import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserServicesService } from '../services/user-services.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(private userService: UserServicesService, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const url: string = state.url;

        return this.checkLogin(url);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    checkLogin(url: string): boolean {

        console.log('Checking login status of user...they are logged in = ' + this.userService.isLoggedIn);

        // If the user is logged in, then we do not need to redirect them and they can proceed to the URL
        if (this.userService.isLoggedIn) { return true; }

        // Otherwise, if they're not logged in, store the URL they attempted to reach for redirecting later
        this.userService.redirectUrl = url;

        // Display the login dialog
        this.router.navigate(['/login']);

        return false;
    }
}
