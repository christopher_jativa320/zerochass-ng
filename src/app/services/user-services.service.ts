import { Injectable, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class UserServicesService {

  // Contains state of the user's login status
  isLoggedIn = new BehaviorSubject(false);

  // Store the URL a user navigated to that required authentication
  redirectUrl: String;

  constructor(public af: AngularFireAuth) {
    this.af.authState
      .subscribe(data => {
        if (data) {
          this.isLoggedIn.next(true);
        } else {
          this.isLoggedIn.next(false);
        }

      }, (err) => { });
  }

  // Handle actions after user has logged in
  loginWithEmailAndPassword(email, password) {
    return this.af.auth.signInWithEmailAndPassword(email, password);
  }

  // Handle user being logged out
  logout() {
    return this.af.auth.signOut();
  }


  // Handle signing the user up with a provided email and password
  signUpWithEmailAndPassword(email, name, password) {
    return this.af.auth.createUserWithEmailAndPassword(email, password);
  }
}


