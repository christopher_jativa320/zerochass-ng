import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { createClient, Entry } from 'contentful';
import { CONFIG } from '../services/contentful-config';

@Injectable()
export class ContentfulService {

  private cdaClient = createClient({
    space: CONFIG.space,
    accessToken: CONFIG.accessToken
  });

  constructor(private http: HttpClient) {}

  getContentfulTutorials(): Observable<any> {
    return this.http.get('/api/tutorials') 
  }

  getContentfulTutorial(slug: string): Observable<any> {
    const options = { params: new HttpParams().set('slug', slug) };
    let response = this.http.get('/api/contentful' + '/' + slug);
    return response;
  }

  getTutorials(query?: object): Promise<Entry<any>[]> {
    return this.cdaClient.getEntries(Object.assign({
      content_type: CONFIG.contentTypeIds.tutorial,
    }, query))
    .then(res => res.items);
  }

  getTutorial(slug: string): Promise<Entry<any>> {
    return this.getTutorials({ 'fields.slug': slug })
    .then(items => items[0]);
  }

}
