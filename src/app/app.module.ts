// Import Angular modules
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Import feature nodules
import { ProjectsModule } from './modules/projects/projects.module';
import { TutorialsModule } from './modules/tutorials/tutorials.module';
import { UserModule } from './modules/user/user.module';

// Import Angular Material wrapper module
import { AppMaterialComponentsModule } from './app-material-components.module';

// Import routing module for our application
import { AppRoutingModule } from './app-routing.module';

// Import providers
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { UserServicesService } from './services/user-services.service';
import { environment } from '../environments/environment';
import { ContentfulService } from './services/contentful.service';


// Import our components
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { BannerComponent } from './components/banner/banner.component';
import { ResourcesBannerComponent } from './components/resources-banner/resources-banner.component';
import { FooterComponent } from './components/footer/footer.component';
import { LowerFooterComponent } from './components/lower-footer/lower-footer.component';
import { SignInComponent } from './components/authentication/sign-in/sign-in.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { MdToHtmlPipe } from './pipes/md-to-html.pipe';
import { HttpClient } from 'selenium-webdriver/http';
import { TutorialService } from './services/tutorial.service';



@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    PageNotFoundComponent,
    BannerComponent,
    ResourcesBannerComponent,
    FooterComponent,
    LowerFooterComponent,
    SignInComponent,
    HomePageComponent,
    MdToHtmlPipe
  ],
  imports: [
    AppMaterialComponentsModule,
    BrowserModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    TutorialsModule,
    ProjectsModule,
    UserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule

  ],
  providers: [UserServicesService, ContentfulService, TutorialService],
  entryComponents: [SignInComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
