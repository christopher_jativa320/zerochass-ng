import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserPageComponent } from './components/user-page/user-page.component';
import { AccountComponent } from './account/account.component';
import { ConnectComponent } from './connect/connect.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from '../../services/auth-guard.service';
import { UserServicesService } from '../../services/user-services.service';
import { SignInComponent } from '../../components/authentication/sign-in/sign-in.component';


const userRoutes: Routes = [
    {
        path: 'user',
        component: UserPageComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                canActivateChild: [AuthGuard],
                children: [
                    { path: 'profile', component: ProfileComponent },
                    { path: 'account', component: AccountComponent },
                    { path: 'connect', component: ConnectComponent }
                ]
            },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(userRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        AuthGuard,
        UserServicesService
    ]
})

export class UserRoutingModule { }
