import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './account/account.component';
import { ConnectComponent } from './connect/connect.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { UserRoutingModule } from './user-routing.module';
import { UserPageComponent } from './components/user-page/user-page.component';
import { AppMaterialComponentsModule } from './../../app-material-components.module';

@NgModule({
  imports: [
    CommonModule, UserRoutingModule, AppMaterialComponentsModule, ReactiveFormsModule
  ],
  exports: [AccountComponent, ConnectComponent, ProfileComponent],
  declarations: [ProfileComponent, AccountComponent, ConnectComponent, SideMenuComponent, UserPageComponent]
})
export class UserModule { }
