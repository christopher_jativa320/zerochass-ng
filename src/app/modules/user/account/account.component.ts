import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  // Form for entering and updating user account information
  accountInfoForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {

    this.createForm();
   }

   createForm() {
    // Build the user account form
    this.accountInfoForm = this.formBuilder.group({
      'email-address': [null, Validators.compose([
        Validators.minLength(3), Validators.maxLength(120)]
      )],
      'password': [null, Validators.compose([
        Validators.required, Validators.minLength(6), Validators.maxLength(60)
      ])],
      'confirm-password': [null, Validators.compose([
        Validators.minLength(6), Validators.maxLength(60)]
      )]
    });
  }


  ngOnInit() {
  }

}
