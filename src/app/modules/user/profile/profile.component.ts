import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  // Form for entering and updating user information
  profileInfoForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {

    this.createForm();

  }

  createForm() {
    // Build the user form
    this.profileInfoForm = this.formBuilder.group({
      'heading': [null, Validators.compose([
        Validators.minLength(3), Validators.maxLength(65)]
      )],
      'name': [null, Validators.compose([
        Validators.required, Validators.minLength(6), Validators.maxLength(60)
      ])],
      'about-me': [null, Validators.compose([
        Validators.minLength(10), Validators.maxLength(300)]
      )],
      'website': [null]
    });
  }

  submitForm() {

    let response;

    const heading = this.profileInfoForm.controls['heading'].value;
    const name = this.profileInfoForm.controls['name'].value;
    const about_me = this.profileInfoForm.controls['about-me'].value;
    const website = this.profileInfoForm.controls['website'].value;

    this.http
      .post('/api/profile', { heading: heading, name: name, about_me: about_me, website: website })
      .subscribe(data => response = data);

  }

  ngOnInit() {
  }

}
