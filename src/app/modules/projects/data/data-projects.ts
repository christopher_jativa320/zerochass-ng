import { Project } from '../classes/project';

export const PROJECTS: Project [] = [
    {
        name: "Serendip-o-matic",
        description: "A simple and lightweight OCR application for grabbing text from images and sending it off to the Serendip-o-matic search engine",
        date_posted: "10/24/2017",
        author: "Christopher D. Jativa",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        repository: "https://www.github.com",
        download_url: "https://www.zerochass.com/",
        tags: ["Android","OCR","Mobile"]
    },
    {
        name: "Exposure",
        description: "An interactive and fully-featured OCR application for grabbing text from images and sending it off to a multitude of search engines",
        date_posted: "10/24/2017",
        author: "Christopher D. Jativa",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        repository: "https://www.github.com",
        download_url: "https://www.zerochass.com/",
        tags: ["Android","OCR","Mobile"]
    }
]

