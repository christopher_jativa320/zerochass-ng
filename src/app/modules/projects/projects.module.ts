import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectComponent } from './components/project/project.component';
import { ProjectsService } from './services/projects.service';
import { ProjectsRoutingModule } from './projects-routing.module';

@NgModule ({
    imports: [CommonModule, ProjectsRoutingModule],
    declarations: [ProjectsComponent,ProjectComponent],
    providers: [ProjectsService]
})

export class ProjectsModule {}