import { Injectable } from '@angular/core';
import { Project } from '../classes/project';
import { PROJECTS } from '../data/data-projects';

@Injectable()
export class ProjectsService {

  constructor() { }

    getProjects(): Promise<Project[]> {
      return Promise.resolve(PROJECTS);
    }

}
