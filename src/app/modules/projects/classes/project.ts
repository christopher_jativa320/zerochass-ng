export class Project {
    name: string;
    description: string;
    date_posted: string;
    author: string;
    content: string;
    repository: string;
    download_url: string;
    tags: string[];
}
