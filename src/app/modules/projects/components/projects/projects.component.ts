import { Component, OnInit } from '@angular/core';
import { Project } from '../../classes/project';
import { ProjectsService } from '../../services/projects.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  providers: [ProjectsService]
})
export class ProjectsComponent implements OnInit {

  projects: Project[];

  constructor(private projectsService: ProjectsService) { }

  getProjects(): void {
    this.projectsService.getProjects().then(projects => this.projects = projects);
  }

  ngOnInit() {
    this.getProjects();
  }

}
