import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TutorialsSectionComponent } from './components/tutorials-section/tutorials-section.component';
import { TutorialComponent } from './components/tutorial/tutorial.component';
import { TutorialsListPageComponent } from './components/tutorials-list-page/tutorials-list-page.component';

const tutorialsRoutes: Routes = [
    { path: 'tutorials', component: TutorialsListPageComponent },
    { path: 'tutorials/:slug', component: TutorialComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(tutorialsRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class TutorialsRoutingModule {}
