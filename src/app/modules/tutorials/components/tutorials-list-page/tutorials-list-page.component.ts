import { Component, OnInit } from '@angular/core';
import { ContentfulService } from '../../../../services/contentful.service';
import { Entry } from 'contentful';

@Component({
  selector: 'app-tutorials-page',
  templateUrl: './tutorials-list-page.component.html',
  styleUrls: ['./tutorials-list-page.component.css']
})
export class TutorialsListPageComponent implements OnInit {

  private tutorials: Entry<any>[] = [];

  constructor(private contentfulService: ContentfulService) { }

  ngOnInit() {
    this.contentfulService.getTutorials()
    .then(tutorials => this.tutorials = tutorials);
  }

}
