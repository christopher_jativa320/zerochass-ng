import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialsListPageComponent } from './tutorials-list-page.component';

describe('TutorialsPageComponent', () => {
  let component: TutorialsListPageComponent;
  let fixture: ComponentFixture<TutorialsListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialsListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
