import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import { ContentfulService } from '../../../../services/contentful.service';
import { Entry } from 'contentful';
import * as Marked from 'marked';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css']
})
export class TutorialComponent implements OnInit {

  tutorial: Entry<any>;
  results;
  tutorialHTML;
  content;

  tags;

  constructor(private contentfulService: ContentfulService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.contentfulService.getContentfulTutorial(params.get('slug')))
      .subscribe(tutorial => {
        this.tutorial = tutorial;
        this.content = Marked(this.tutorial.fields.content);
         this.parseContentSections();
      });
  }

  parseContentSections() {
    this.results = this.content.split('<hr>');
    for (let i = 0; i < this.results.length; i++) {
      //this.tutorialHTML += this.results[i];
      //this.tutorialHTML += '<p>Then a new section</p>';
    }
  }

}
