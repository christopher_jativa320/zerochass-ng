import { Component, OnInit } from '@angular/core';
import { ContentfulService } from '../../../../services/contentful.service';
import { TutorialService } from '../../../../services/tutorial.service';
import { Entry } from 'contentful';


@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials-section.component.html',
  styleUrls: ['./tutorials-section.component.css'],
  providers: [ContentfulService]
})
export class TutorialsSectionComponent implements OnInit {

  private tutorials: Entry<any>[] = [];
  tutorialID: string;

  constructor(private contentfulService: ContentfulService, private tutorialService: TutorialService) {
    this.getTutorials();
  }

  getTutorials(): void {
    this.contentfulService.getContentfulTutorials()
      .subscribe(data => {
        this.tutorials = data
      })
  }

  passTutorialId(id: string): void {
    console.log(id);
  }

  ngOnInit() {
  }

}
