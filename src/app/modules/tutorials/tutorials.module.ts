import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TutorialsSectionComponent } from './components/tutorials-section/tutorials-section.component';
import { TutorialComponent } from './components/tutorial/tutorial.component';
import { TutorialsService } from './services/tutorials.service';
import { TutorialsRoutingModule } from './tutorials-routing.module';
import { TutorialsMaterialComponentsModule } from './tutorials-material-components.module';
import { TutorialBannerHighlightDirective } from '../../directives/tutorial-banner-highlight.directive';
import { TutorialsListPageComponent } from './components/tutorials-list-page/tutorials-list-page.component';

@NgModule ({
    imports: [CommonModule, TutorialsRoutingModule, TutorialsMaterialComponentsModule],
    exports: [TutorialsSectionComponent],
    declarations: [TutorialsSectionComponent, TutorialComponent, TutorialBannerHighlightDirective, TutorialsListPageComponent],
    providers: [TutorialsService]
})

export class TutorialsModule {}
