import { NgModule } from '@angular/core';

import {MatToolbarModule, MatButtonModule, MatInputModule, MatListModule} from '@angular/material';

@NgModule({
  imports: [MatToolbarModule, MatButtonModule, MatInputModule, MatListModule],
  exports: [MatToolbarModule, MatButtonModule, MatInputModule, MatListModule],
})
export class TutorialsMaterialComponentsModule { }