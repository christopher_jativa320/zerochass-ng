export class Tutorial {
    title: string;
    sub_title: string;
    id: string;
    date_posted: string;
    author: string;
    content: string;
    tags: string[];
}
