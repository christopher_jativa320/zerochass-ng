import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tutorial } from '../classes/tutorial';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TutorialsService {

  constructor(private http: HttpClient) { }

  getTutorials(): Observable<Tutorial[]> {
    return this.http.get<Tutorial []>('/api/tutorials');
  }

  getTutorial(id: string): Observable<Tutorial> {
    return this.http.get<Tutorial>('/api/tutorial/' + `${id}`);
  }

}
