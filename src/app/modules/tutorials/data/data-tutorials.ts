import { Tutorial } from '../classes/tutorial';

export const TUTORIALS: Tutorial[] = [
    {
        title: 'Creating a Simple Google Search Application in Java',
        sub_title: 'Check out this tutorial to get an understanding of the foundations of Computer Science through the popular programming language known as Java',
        id: 'creating-a-simple-google-search-application-in-java',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice. Further steps will be written.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development']
    },
    {
        title: 'Capturing Text From Images Using OCR',
        sub_title: 'Read on to create an OCR-powered Android application',
        id: 'capturing-text-from-images-using-ocr',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop for Android. Further steps will be written.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    },
    {
        title: 'Building a Hello World Application',
        sub_title: 'Read on to create an flexible Hello World application',
        id: 'building-a-hello-world-application',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop using Javascript.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    },
    {
        title: 'Building a New World Application',
        sub_title: 'Read on to create an flexible Hello World application',
        id: 'building-a-new-world-application',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop using Javascript.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    },
    {
        title: 'Building a My World Application',
        sub_title: 'Read on to create an flexible Hello World application',
        id: 'building-a-my-world-application',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop using Javascript.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    },
    {
        title: 'Building a Game Application',
        sub_title: 'Read on to create an flexible Hello World application',
        id: 'building-a-game-world-application',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop using Javascript.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    },
    {
        title: 'Building a Machine Learning Application',
        sub_title: 'Read on to create an flexible Hello World application',
        id: 'building-a-machine-learning-application',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop using Javascript.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    },
    {
        title: 'Building an iOS Application',
        sub_title: 'Read on to create an flexible Hello World application',
        id: 'building-an-ios-application',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop using Javascript.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    },
    {
        title: 'Building a Linux Application',
        sub_title: 'Read on to create an flexible Hello World application',
        id: 'building-a-linux-application',
        date_posted: '10/24/2017',
        author: 'Christopher D Jativa',
        content: 'First download an IDE of your choice to develop using Javascript.',
        tags: ['Java', 'Computer Science', 'Software Engineering', 'Software Development', 'Android', 'Android Development']
    }
];
